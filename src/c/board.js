import React, { Component } from 'react';
import ListItem from './listItem';
import NewItem from './newItem';

class Board extends Component {

    constructor(props) {
        super(props);
        this.state = {
            addNew: false,
        }
        this.onAdd = this.onAdd.bind(this);
        this.submit = this.submit.bind(this);
        this.cancel = this.cancel.bind(this);
    }

    onAdd() {
        this.setState({
            addNew: true
        });
    }

    submit(text){
        const cat = this.props.data.id;
        this.props.addTask(text, cat);
        this.setState({
            addNew: false
        });
    }

    cancel(){
        this.setState({
            addNew: false
        });
    }

  render() {
      const {onDragItem, onDropItem, data} = this.props;
      const {tasks, id, name} = data;
      const ListUI = tasks.length > 0 ?
        tasks.map((el)=>{
            return (<ListItem data={el} cat={id} onDragItem={onDragItem} onDropItem={onDropItem}  />)
        }) : null;
    return (
      <div className="board">
        <h3>{name}</h3>
        <div className="list" 
        >
            {ListUI}
        </div>
        {!this.state.addNew &&
            <div className="add-task" onClick={this.onAdd}>+ Add a task</div>
        }
        {this.state.addNew &&
            <NewItem add={this.submit} cancel={this.cancel} />
        }
      </div>
    );
  }
}

export default Board;
