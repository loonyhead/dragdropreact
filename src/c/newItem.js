import React, { Component } from 'react';

class NewItem extends Component {

    constructor(props) {
        super(props);
        this.state = {
            text : null,
        }
        this.onChangeHandler = this.onChangeHandler.bind(this);
        this.add = this.add.bind(this);
        this.cancel = this.cancel.bind(this);
    }

    onChangeHandler(e){
        this.setState({
            text: e.target.value
        });
    }

    add(){
        if(this.state.text && this.state.text.length > 0){
            this.props.add(this.state.text);
        }
    }

    cancel(){
        this.props.cancel();
    }

  render() {
    return (
      <div className="addItem">
        <div>
            <input name="newtask" type={Text} onChange={this.onChangeHandler} />
        </div>
        <div>
            <button className="addbtn" onClick={this.add}>Add</button>
            <button className="cancelbtn" onClick={this.cancel}>Cancel</button>
        </div>
      </div>
    );
  }
}

export default NewItem;
