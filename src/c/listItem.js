import React, { Component } from 'react';

class ListItem extends Component {

    constructor(props) {
        super(props);
        this.onDrag = this.onDrag.bind(this);
        this.onDrop = this.onDrop.bind(this);
        this.onDragOver = this.onDragOver.bind(this);
    }

    onDrag(v, c){
        this.props.onDragItem(c, v);
    }

    onDrop(e, v, c){
        this.props.onDropItem(c, v);
    }

    onDragOver(e){
        e.preventDefault();
    }

  render() {
      const {data, cat} = this.props;
    return (
      <div className="listItem"
         draggable
         onDragStart={()=>this.onDrag(data, cat)}    
         onDrop={(e)=>this.onDrop(e, data, cat)}
         onDragOver={(e)=>this.onDragOver(e)}
        >
        {data.value}
      </div>
    );
  }
}

export default ListItem;
