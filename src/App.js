import React, { Component } from 'react';
import './App.css';
import Board from './c/board';

class App extends Component {
  constructor(props) {
    super(props);
    this.state = {
      board : [
        {
          id : 1,
          name : "Todo",
          tasks : []
        },{
          id : 2,
          name : "Doing",
          tasks : []
        },{
          id : 3,
          name : "Done",
          tasks : []
        }
      ],
      draggedItem : null,
      droppedItem : null,
    };
    this.onDragItem = this.onDragItem.bind(this);
    this.onDropItem = this.onDropItem.bind(this);
    this.addTask = this.addTask.bind(this);
    this.onDragOver= this.onDragOver.bind(this);
    this.dropHandler = this.dropHandler.bind(this);
  }

  addTask(text, cat){
    let addBoards = this.state.board;
    let currentList = addBoards.filter(itm=>itm.id===cat)[0];
    let currentTasks = currentList.tasks;
    const index = addBoards.findIndex(x=>x.id===cat);
    currentTasks.push({
      id: Math.random(), //can be replaced with uid
      value: text
    });
    currentList = Object.assign({}, currentList, {tasks: currentTasks});
    addBoards[index] = currentList;
    this.setState({
      board: addBoards
    });
  }

  onDragItem(id, value){
    this.setState({
      draggedItem : {
        id,
        value,
      }
    });
  }

  dropHandler(cat, value=null){
    const {board, draggedItem} = this.state;
    const draggedCat=draggedItem.id;
    const draggedValue=draggedItem.value;
    if(value && value.id === draggedValue.id){
      return;
    }
    let droppedObj, draggedObj, droppedObjTasks, draggedObjTasks, draggedIndex, droppedIndex;
    droppedObj=board.filter(itm=>itm.id===cat)[0];
    draggedObj=board.filter(itm=>itm.id===draggedCat)[0];
    droppedObjTasks=droppedObj.tasks;
    draggedObjTasks=draggedObj.tasks;
    draggedIndex=draggedObjTasks.findIndex(x=>x.id===draggedValue.id);
    if(!value){
      droppedIndex=droppedObjTasks.length;
    }else{
      droppedIndex=droppedObjTasks.findIndex(x=>x.id===value.id);
    }
    droppedObjTasks.splice(droppedIndex, 0, draggedValue);
    draggedObjTasks.splice(draggedIndex, 1);
    droppedObj=Object.assign({}, droppedObj, {tasks : droppedObjTasks});
    if(draggedCat !== cat){
      draggedObj=Object.assign({}, draggedObj, {tasks : draggedObjTasks});
    }
    let newBoard = [];
    for(let i=0; i<board.length; i++){
      if(board[i].id === cat){
        newBoard[i]=droppedObj;
      }else if(board[i].id===draggedCat && draggedCat!==cat){
        newBoard[i] = draggedObj;
      }else{
        newBoard[i] = board[i];
      }
    }
    this.setState({
        board : newBoard
    });
  }

  onDropItem(id, value){
    this.itemDropped = true;
    this.setState({
      droppedItem : {
        id,
        value,
      }
    }, ()=>this.dropHandler(id, value));
  }

  onDragOver(e){
    e.preventDefault();
  }

  onDropInCont(id){
    if(this.itemDropped){
      this.itemDropped = false;
    }else{
      this.dropHandler(id);
    }
  }

  render() {
    const BoardUi = this.state.board.map((el)=>{
      return (
        <div onDragOver={(e)=>this.onDragOver(e)} onDrop={(e)=>this.onDropInCont(el.id)} className="boardList">
          <Board data={el} onDragItem={this.onDragItem} onDropItem={this.onDropItem} addTask={this.addTask} />
        </div>
      )
    });
    return (
      <div className="App">
        {BoardUi}
      </div>
    );
  }
}

export default App;
